export class PenshiruPage {
  navigateTo() {
    return browser.get('/');
  }

  getParagraphText() {
    return element(by.css('penshiru-app h1')).getText();
  }
}
