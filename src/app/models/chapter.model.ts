import {Article} from './article.model';

export class Chapter{
    id: number;
    name: string;
    chapters: Article[];
    titleID: number;
}