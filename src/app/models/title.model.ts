import {Chapter} from './chapter.model'

export class Title{
    id: number;
    name: string;
    chapters: Chapter[];
    lawID: number;
}