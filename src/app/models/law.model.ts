import {Title} from './title.model';

export class Law{
    id: number;
    name: string;
    titles: Title[];
    approvalDate: Date;
    publishData: Date;
    journal: string;
    intro: string;
}