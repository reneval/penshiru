import { Component, OnInit } from '@angular/core';
import {MD_LIST_DIRECTIVES} from '@angular2-material/list';
import {MD_CARD_DIRECTIVES} from '@angular2-material/card';



@Component({
  moduleId: module.id,
  selector: 'app-law-review',
  templateUrl: 'law-review.component.html',
  styleUrls: ['law-review.component.css'],
  directives:[MD_LIST_DIRECTIVES, MD_CARD_DIRECTIVES]
})
export class LawReviewComponent implements OnInit {

  id:number;

  constructor() {}

  ngOnInit() {
  }
  
  
  //dummy
  laws: Object[] = [
    {
      name: "Ley de Concertacion Tributaria",
      date: "23/11/84",
      user: "reneval",
      rev:"01"
    },
    {
      name: "Ley de Justicia Tributaria",
      date: "18/02/85",
      user: "jaime",
      rev:"03"
    },
    
  ];
  

}
