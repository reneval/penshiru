import { Component, OnInit } from '@angular/core';
import { LawUploadComponent } from './+law-upload';
import { LawReviewComponent } from './+law-review';
import { Router, Routes , ROUTER_DIRECTIVES, ROUTER_PROVIDERS} from '@angular/router';

import {MdToolbar} from '@angular2-material/toolbar'
import {MdIcon,MdIconRegistry} from '@angular2-material/icon'
import {MdButton} from '@angular2-material/button';
import {MD_SIDENAV_DIRECTIVES} from '@angular2-material/sidenav'
import {MD_LIST_DIRECTIVES} from '@angular2-material/list';
import {MD_CARD_DIRECTIVES} from '@angular2-material/card';

@Component({
  moduleId: module.id,
  selector: 'penshiru-app',
  templateUrl: 'penshiru.component.html',
  styleUrls: ['penshiru.component.css'],
  directives:[
    LawUploadComponent,
    MdToolbar,
    MdIcon,
    MdButton,
    MD_SIDENAV_DIRECTIVES,
    MD_LIST_DIRECTIVES,
    MD_CARD_DIRECTIVES,
    ROUTER_DIRECTIVES
    ],
    providers:[MdIconRegistry,ROUTER_PROVIDERS ]
})
@Routes([
  {path: '/law-review', component: LawReviewComponent},
  {path: '/law-review/:id', component: LawReviewComponent},
  {path: '/law-upload', component: LawUploadComponent},
])
export class PenshiruAppComponent implements OnInit {
  title = 'Penshiru';
  
  views: Object[] = [
    {
      name: "My Account",
      description: "Edit my account information",
      icon: "assignment ind"
    },
    {
      name: "Laws:",
      description: "Find your soulmate!",
      icon: "book"
    }
  ];
  
  constructor(private router: Router) {}

  
  ngOnInit(){
    // this.router.navigate(['/law-upload']);
  }
}
