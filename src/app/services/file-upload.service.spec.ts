import {
  beforeEachProviders,
  it,
  describe,
  expect,
  inject
} from '@angular/core/testing';
import { FileUploadService } from './file-upload.service';

describe('Upload Service', () => {
  beforeEachProviders(() => [FileUploadService]);

  it('should ...',
      inject([FileUploadService], (service: FileUploadService) => {
    expect(service).toBeTruthy();
  }));
});
