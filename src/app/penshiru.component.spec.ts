import {
  beforeEachProviders,
  describe,
  expect,
  it,
  inject
} from '@angular/core/testing';
import { PenshiruAppComponent } from '../app/penshiru.component';

beforeEachProviders(() => [PenshiruAppComponent]);

describe('App: Penshiru', () => {
  it('should create the app',
      inject([PenshiruAppComponent], (app: PenshiruAppComponent) => {
    expect(app).toBeTruthy();
  }));

  it('should have as title \'penshiru works!\'',
      inject([PenshiruAppComponent], (app: PenshiruAppComponent) => {
    expect(app.title).toEqual('penshiru works!');
  }));
});
